$(document).ready(function () {
	//상단 배너 스크롤 이벤트
		//var lastScrollTop = 0, delta = 15;
		$(window).scroll(function(event){
			var st = $(this).scrollTop();
			/*if(Math.abs(lastScrollTop - st) <= delta)
				return;*/
			if(st > 0){
				$('#header').addClass('fix');
			}else{
				$('#header').removeClass('fix');
			}
			/*if ((st > lastScrollTop) && (lastScrollTop > $('#header_warp .header_box').outerHeight() )) {
			}*/
				//lastScrollTop = st;
		});

	//메인 상단 설문조사
	$('.hd_member > ul > li.btn_survey').click(function(){
		$('.survey_pop.main_survey').css('display', 'block');
		$('body').css('overflow', 'hidden');
		$('#layer_dim').removeClass('dn');
	});

	$('.survey_pop.main_survey .pop_close, .survey_pop.main_survey .survey_box > a, #layer_dim').click(function(){
		$('.survey_pop.main_survey').css('display', 'none');
		$('body').css('overflow', 'initial');
		$('#layer_dim').addClass('dn');
	});

	//설문조사 체크 다 하지 않았을 때 (임시)
	$('.survey_content .no_chk_btn').click(function(){
		$('.survey_pop.no_chk').css('display', 'block');
		$('body').css('overflow', 'hidden');
	});
	$('.survey_pop.no_chk a').click(function(){
		$('.survey_pop.no_chk').css('display', 'none');
		$('body').css('overflow', 'initial');
	});

	//설문조사 체크 다 했을 때 (임시)
	$('.survey_content .btn_survey_submit').click(function(){
		$('.survey_pop.survey_fin').css('display', 'block');
		$('body').css('overflow', 'hidden');
		$('#layer_dim').removeClass('dn');
	});
	$('.survey_pop.survey_fin a, .survey_pop.survey_fin .pop_close').click(function(){
		$('.survey_pop.survey_fin').css('display', 'none');
		$('body').css('overflow', 'initial');
		$('#layer_dim').addClass('dn');
	});
		
});
