$(document).ready(function(){
	//SCROLL DOWN
		$('.sc_down').click(function(){
			var MainScDown = $('.main_class').offset().top;
			$('html, body').stop().animate( { scrollTop : MainScDown }, 800 );
		});
	//메인비주얼 슬라이드
		var sliderTimer = 7000;
		var visualtime = 3;
		var $bar,
				isPause,
				tick,
				percentTime;
		var slideCount = $('.main_visual .slideCount');
		var visualSlider = $('.main_visual_slide');
		visualSlider.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {//currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
			var i = (currentSlide ? currentSlide : 0) + 1;
			$('.main_visual').find('.slideCountItem').html('0' + i + '');
			$('.main_visual').find('.slideCountAll').html('0' + slick.slideCount + '');
		});
		visualSlider.slick({
			arrows: true,
			autoplay: false,
			autoplaySpeed: sliderTimer,
			speed: 500,
			draggable: true,
			mobileFirst: true,
			pauseOnHover: true,
			dots: false
		});
		$bar = $('.main_visual .slider-progress .progress');
		//
		function startProgressbar() {
			resetProgressbar();
			percentTime = 0;
			isPause = false;
			tick = setInterval(interval, 15);
		}
		function interval() {
			if (isPause === false) {
				percentTime += 1 / (visualtime + 0.1);
				$bar.css({ width: percentTime + "%" });
				if (percentTime >= 100) {
					visualSlider.slick('slickNext');
					resetProgressbar();
				}
			}
		}
		function resetProgressbar() {
			$bar.css({ width: 0 + '%' });
			clearTimeout(tick);
		}
		visualSlider.on('afterChange', function(event, slick, currentSlide) {
			startProgressbar();
		});
		startProgressbar();
});
